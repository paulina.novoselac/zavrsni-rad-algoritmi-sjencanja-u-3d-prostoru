#version 330 core

in vec3 FragPos;    // Pozicija fragmenta u svijetu
in vec3 Normal;     // Normala fragmenta u svijetu

out vec4 finalColor;  // Kona�na boja piksela

uniform vec3 lightPosition;   // Pozicija svjetla
uniform vec3 viewPos;    // Pozicija kamere
uniform vec3 lightColor; // Boja svjetla
uniform vec3 ambientColor; // Ambijentalna refleksija materijala
uniform vec3 diffuseColor; // Difuzna refleksija materijala
uniform vec3 specularColor; // Spekularna refleksija materijala
uniform float shininess; // Bljesak materijala

void main()
{
    //  smjer svjetlosti
    vec3 lightDir = normalize(lightPosition - FragPos);

    //  difuzna komponentu osvjetljenja
    float diff = max(dot(Normal, lightDir), 0.0);

    //  spekularna komponentu osvjetljenja
    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 reflectDir = reflect(-lightDir, Normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.5), shininess);

    //  kona�na boju piksela koriste�i Phongov model 
    vec3 ambient = ambientColor * lightColor;
    vec3 diffuse = diffuseColor * diff * lightColor;
    vec3 specular = specularColor * spec * lightColor;

    vec3 result = (ambient+ diffuse + specular);

    //  kona�na boju piksela
    finalColor = vec4(result, 1.0);
}
