#vercsion 330 core

in vec3 FragColor; //Ulazna boja iz vertex shadera

out vec4 finalColor; //Izlazna boja fragment shadera

void main(){
	finalColor = vec4 (FragColor, 1.0); //Postavljamo boju fragmenta na boju iz vertex shadera
}
