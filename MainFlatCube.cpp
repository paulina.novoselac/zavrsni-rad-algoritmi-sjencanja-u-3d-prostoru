#include<iostream>
#include<glad/glad.h>
#include<GLFW/glfw3.h> 
#include<stb/stb_image.h>
#include<glm/glm.hpp>
#include<glm/gtc/matrix_transform.hpp>
#include<glm/gtc/type_ptr.hpp>
#include <vector>
#include <cmath>

#include"shaderClass.h"
#include"VAO.h"
#include"VBO.h"
#include"EBO.h"

const unsigned int width = 800;
const unsigned int height = 800;

const int numVertices = 24;  
const int numIndices = 36;   

float cubeVertices[] = {

	-0.5f, -0.5f, 0.5f, 	1.0f, 0.0f, 0.0f,  
	 0.5f, -0.5f, 0.5f, 	1.0f, 0.0f, 0.0f,  
	 0.5f,  0.5f, 0.5f, 	1.0f, 0.0f, 0.0f,  
	-0.5f,  0.5f, 0.5f, 	1.0f, 0.0f, 0.0f,  

	-0.5f, -0.5f, -0.5f, 	0.0f, 1.0f, 0.0f,  
	 0.5f, -0.5f, -0.5f,  	0.0f, 1.0f, 0.0f,  
	 0.5f,  0.5f, -0.5f,  	0.0f, 1.0f, 0.0f, 
	-0.5f,  0.5f, -0.5f,  	0.0f, 1.0f, 0.0f,  

	 0.5f, -0.5f,  0.5f,  	0.0f, 0.0f, 1.0f,  
	 0.5f, -0.5f, -0.5f, 	0.0f, 0.0f, 1.0f,  
	 0.5f,  0.5f, -0.5f, 	0.0f, 0.0f, 1.0f,  
	 0.5f,  0.5f,  0.5f, 	0.0f, 0.0f, 1.0f,  

	-0.5f, -0.5f,  0.5f, 	1.0f, 1.0f, 0.0f,  
	-0.5f, -0.5f, -0.5f, 	1.0f, 1.0f, 0.0f,  
	-0.5f,  0.5f, -0.5f,  	1.0f, 1.0f, 0.0f,  
	-0.5f,  0.5f,  0.5f, 	1.0f, 1.0f, 0.0f,  

	-0.5f,  0.5f,  0.5f,  	1.0f, 0.0f, 1.0f,  
	 0.5f,  0.5f,  0.5f,  	1.0f, 0.0f, 1.0f,  
	 0.5f,  0.5f, -0.5f, 	1.0f, 0.0f, 1.0f,  
	-0.5f,  0.5f, -0.5f, 	1.0f, 0.0f, 1.0f,  

	-0.5f, -0.5f,  0.5f, 	0.0f, 1.0f, 1.0f,  
	 0.5f, -0.5f,  0.5f, 	0.0f, 1.0f, 1.0f,  
	 0.5f, -0.5f, -0.5f, 	0.0f, 1.0f, 1.0f, 
	-0.5f, -0.5f, -0.5f,   	0.0f, 1.0f, 1.0f   
};


unsigned int cubeIndices[] = {

	0, 1, 2,
	2, 3, 0,

	4, 5, 6,
	6, 7, 4,

	8, 9, 10,
	10, 11, 8,

	12, 13, 14,
	14, 15, 12,

	16, 17, 18,
	18, 19, 16,

	20, 21, 22,
	22, 23, 20
};



int main()
{

	glfwInit();

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	GLFWwindow* window = glfwCreateWindow(width, height, "zr_ogl", NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);


	gladLoadGL();
	glViewport(0, 0, width, height);
	Shader shaderProgram("default.vert", "default.frag");
	glGetError();
	VAO VAO1;
	VAO1.Bind();

	VBO VBO1(cubeVertices, sizeof(cubeVertices));
	EBO EBO1(cubeIndices, sizeof(cubeIndices));
	VAO1.LinkAttrib(VBO1, 0, 3, GL_FLOAT, 6 * sizeof(float), (void*)0);  // Pozicije
	VAO1.LinkAttrib(VBO1, 1, 3, GL_FLOAT, 6 * sizeof(float), (void*)(3 * sizeof(float)));  // Boje

	VAO1.Unbind();
	VBO1.Unbind();
	EBO1.Unbind();
	GLuint uniID = glGetUniformLocation(shaderProgram.ID, "scale");

	float rotation = 0.0f;
	double prevTime = glfwGetTime();
	glEnable(GL_DEPTH_TEST);

	while (!glfwWindowShouldClose(window))
	{
		glClearColor(0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		shaderProgram.Activate();

		double crntTime = glfwGetTime();
		if (crntTime - prevTime >= 1 / 60)
		{
			rotation += 0.1f;
			prevTime = crntTime;
		}

		glm::mat4 model = glm::mat4(1.0f);
		glm::mat4 view = glm::mat4(1.0f);
		glm::mat4 proj = glm::mat4(1.0f);

		model = glm::rotate(model, glm::radians(rotation), glm::vec3(0.5f, 1.0f, 1.0f));
		view = glm::translate(view, glm::vec3(0.0f, 0.0f, -2.0f));
		proj = glm::perspective(glm::radians(90.0f), (float)width / height, 0.1f, 100.0f);

		int modelLoc = glGetUniformLocation(shaderProgram.ID, "model");
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		int viewLoc = glGetUniformLocation(shaderProgram.ID, "view");
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
		int projLoc = glGetUniformLocation(shaderProgram.ID, "proj");
		glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(proj));

		glUniform1f(uniID, 0.5f);
		VAO1.Bind();
		glDrawElements(GL_TRIANGLES, sizeof(cubeIndices), GL_UNSIGNED_INT, 0);
		glfwSwapBuffers(window);
		glGetError();
		glfwPollEvents();
	}

	VAO1.Delete();
	VBO1.Delete();
	EBO1.Delete();
	shaderProgram.Delete();
	glfwDestroyWindow(window);
	glfwTerminate();
	return 0;
}