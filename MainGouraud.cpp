#include<iostream>
#include<glad/glad.h>
#include<GLFW/glfw3.h> 
#include<stb/stb_image.h>
#include<glm/glm.hpp>
#include<glm/gtc/matrix_transform.hpp>
#include<glm/gtc/type_ptr.hpp>
#include <vector>
#include <cmath>

#include"shaderClass.h"
#include"VAO.h"
#include"VBO.h"
#include"EBO.h"


const unsigned int width = 800;
const unsigned int height = 800;

const float PI = 3.14159265359f;

void createSphere(float*& vertices, float radius, int stacks, int slices) {
	int numVertices = (stacks + 1) * (slices + 1);
	int verticesSize = 6 * numVertices;
	vertices = new GLfloat[verticesSize];

	// Vektor za pohranu normala za svaki vrh
	std::vector<glm::vec3> vertexNormals(numVertices, glm::vec3(0.0f));

	int vIndex = 0;

	for (int i = 0; i <= stacks; ++i) {
		float phi = PI * static_cast<float>(i) / static_cast<float>(stacks);
		for (int j = 0; j <= slices; ++j) {
			float theta = 2.0f * PI * static_cast<float>(j) / static_cast<float>(slices);

			GLfloat x = radius * std::sin(phi) * std::cos(theta);
			GLfloat y = radius * std::cos(phi);
			GLfloat z = radius * std::sin(phi) * std::sin(theta);

			vertices[vIndex++] = x;
			vertices[vIndex++] = y;
			vertices[vIndex++] = z;

			GLfloat r = 1.0f; 
			GLfloat g = 1.0f;
			GLfloat b = 1.0f;

			vertices[vIndex++] = r;
			vertices[vIndex++] = g;
			vertices[vIndex++] = b;

			// Normala trenutnog vrha
			glm::vec3 normal(x, y, z);
			vertexNormals[(i * (slices + 1)) + j] = glm::normalize(normal);
		}
	}

}


void calculateSphereIndices(unsigned int*& indices, int stacks, int slices) {
	int numIndices = stacks * slices * 6;
	indices = new GLuint[numIndices];

	int iIndex = 0;

	for (int i = 0; i < stacks; ++i) {
		for (int j = 0; j < slices; ++j) {
			int current = i * (slices + 1) + j;
			int next = current + slices + 1;

			// Prvi trokut
			indices[iIndex++] = current;
			indices[iIndex++] = next;
			indices[iIndex++] = current + 1;

			// Drugi trokut
			indices[iIndex++] = current + 1;
			indices[iIndex++] = next;
			indices[iIndex++] = next + 1;
		}
	}
}



int main()
{

	glfwInit();

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	GLFWwindow* window = glfwCreateWindow(width, height, "zr_ogl", NULL, NULL);

	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);


	// Podaci o sferi
	float radius = 0.3f;
	float* vertices;
	unsigned int* indices;
	int stacks = 20;
	int slices = 20;

	createSphere(vertices, radius, stacks, slices);
	calculateSphereIndices(indices, stacks, slices);

	gladLoadGL();

	glViewport(0, 0, width, height);

	Shader shaderProgram("gouraud.vert", "gouraud.frag");

	VAO VAO1;
	VAO1.Bind();

	size_t vertexSize = 6 * sizeof(float);
	std::vector<glm::vec3> vertexNormals;

	VBO VBO1(vertices, (stacks + 1) * (slices + 1) * vertexSize);
	EBO EBO1(indices, stacks * slices * 6 * sizeof(unsigned int));


	VAO1.LinkAttrib(VBO1, 0, 3, GL_FLOAT, vertexSize, (void*)0);                      // Pozicije
	VAO1.LinkAttrib(VBO1, 1, 3, GL_FLOAT, vertexSize, (void*)(3 * sizeof(float)));    // Boja
	VAO1.LinkAttrib(VBO1, 2, 3, GL_FLOAT, vertexSize, (void*)(6 * sizeof(float)));  // Normale

	VAO1.Unbind();
	VBO1.Unbind();
	EBO1.Unbind();


	GLuint uniID = glGetUniformLocation(shaderProgram.ID, "scale");


	float rotation = 0.0f;
	double prevTime = glfwGetTime();

	glEnable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

	while (!glfwWindowShouldClose(window))
	{

		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);//boja pozadine

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		shaderProgram.Activate();


		glm::mat4 model = glm::mat4(1.0f);
		glm::mat4 proj = glm::mat4(1.0f);

		glm::vec3 lightColor = glm::vec3(1.0f, 1.0f, 1.0f);
		glm::vec3 lightPosition(1.0f, 5.0f, 5.0f);
		glm::vec3 ambientColor(0.1f, 0.1f, 0.1f);
		glm::vec3 diffuseColor(0.0f, 0.2f, 1.0f);
		glm::vec3 specularColor(1.0f, 1.0f, 1.0f);
		float shininess = 10.0f;


		model = glm::mat4(1.0f);
		glm::vec3 cameraPosition = glm::vec3(0.0f, 0.0f, 2.0f);
		glm::mat4 view = glm::lookAt(cameraPosition, glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		proj = glm::perspective(glm::radians(45.0f), (float)width / height, 0.1f, 10.0f);


		glUniform3fv(glGetUniformLocation(shaderProgram.ID, "viewPos"), 1, &cameraPosition[0]);


		int modelLoc = glGetUniformLocation(shaderProgram.ID, "model");
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		int viewLoc = glGetUniformLocation(shaderProgram.ID, "view");
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
		int projLoc = glGetUniformLocation(shaderProgram.ID, "proj");
		glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(proj));

		GLint lightColorLocation = glGetUniformLocation(shaderProgram.ID, "lightColor");
		glUniform3fv(lightColorLocation, 1, glm::value_ptr(lightColor));

		GLuint lightPosLocation = glGetUniformLocation(shaderProgram.ID, "lightPosition");
		glUniform3fv(lightPosLocation, 1, glm::value_ptr(lightPosition));

		GLint ambientLoc = glGetUniformLocation(shaderProgram.ID, "ambientColor");
		glUniform3fv(ambientLoc, 1, glm::value_ptr(ambientColor));

		GLint diffuseLoc = glGetUniformLocation(shaderProgram.ID, "diffuseColor");
		glUniform3fv(diffuseLoc, 1, glm::value_ptr(diffuseColor));

		GLint specularLoc = glGetUniformLocation(shaderProgram.ID, "specularColor");
		glUniform3fv(specularLoc, 1, glm::value_ptr(specularColor));

		GLint shininessLoc = glGetUniformLocation(shaderProgram.ID, "shininess");
		glUniform1f(shininessLoc, shininess);


		glUniform1f(uniID, 0.5f);
		VAO1.Bind();
		glDrawElements(GL_TRIANGLES, stacks * slices * 6, GL_UNSIGNED_INT, 0);
		glfwSwapBuffers(window);
		glfwPollEvents();
	}


	VAO1.Delete();
	VBO1.Delete();
	EBO1.Delete();
	shaderProgram.Delete();
	glfwDestroyWindow(window);
	glfwTerminate();
	return 0;
}
