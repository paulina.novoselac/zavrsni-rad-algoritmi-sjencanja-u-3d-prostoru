#include<iostream>
#include<glad/glad.h>
#include<GLFW/glfw3.h> 
#include<stb/stb_image.h>
#include<glm/glm.hpp>
#include<glm/gtc/matrix_transform.hpp>
#include<glm/gtc/type_ptr.hpp>
#include <vector>
#include <cmath>

#include"shaderClass.h"
#include"VAO.h"
#include"VBO.h"
#include"EBO.h"


const unsigned int width = 800;
const unsigned int height = 800;

const float PI = 3.14159265359f;
void createSphere(float*& vertices, float radius, int stacks, int slices) {
	int numVertices = (stacks + 1) * (slices + 1);
	int verticesSize = 6 * numVertices; 
	vertices = new GLfloat[verticesSize];

	int vIndex = 0;

	for (int i = 0; i <= stacks; ++i) {
		float phi = PI * static_cast<float>(i) / static_cast<float>(stacks);
		for (int j = 0; j <= slices; ++j) {
			float theta = 2.0f * PI * static_cast<float>(j) / static_cast<float>(slices);

			GLfloat y = radius * std::sin(phi) * std::cos(theta);
			GLfloat x = radius * std::cos(phi);
			GLfloat z = radius * std::sin(phi) * std::sin(theta);

			vertices[vIndex++] = x;
			vertices[vIndex++] = y;
			vertices[vIndex++] = z;

			GLfloat r = static_cast<GLfloat>(j) / static_cast<GLfloat>(slices); 
			GLfloat g = static_cast<GLfloat>(i) / static_cast<GLfloat>(stacks); 
			GLfloat b = 1.0f - r;


			vertices[vIndex++] = r;
			vertices[vIndex++] = g;
			vertices[vIndex++] = b;
		}
	}
}

void calculateSphereIndices(unsigned int*& indices, int stacks, int slices) {
	int numIndices = stacks * slices * 6;
	indices = new GLuint[numIndices];

	int iIndex = 0;

	for (int i = 0; i < stacks; ++i) {
		for (int j = 0; j < slices; ++j) {
			int current = i * (slices + 1) + j;
			int next = current + slices + 1;

			indices[iIndex++] = current;
			indices[iIndex++] = next;
			indices[iIndex++] = current + 1;

			indices[iIndex++] = current + 1;
			indices[iIndex++] = next;
			indices[iIndex++] = next + 1;
		}
	}
}



int main()
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	GLFWwindow* window = glfwCreateWindow(width, height, "zr_ogl", NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	//Sfera
	float radius = 0.3f;	
	float* vertices;
	unsigned int* indices;
	int stacks = 20;
	int slices = 20;

	createSphere(vertices, radius, stacks, slices);
	calculateSphereIndices(indices, stacks, slices);

	gladLoadGL();
	glViewport(0, 0, width, height);

	Shader shaderProgram("default.vert", "default.frag");

	VAO VAO1;
	VAO1.Bind();

	size_t vertexSize = 6 * sizeof(float); 
	VBO VBO1(vertices, (stacks + 1) * (slices + 1) * vertexSize);
	EBO EBO1(indices, stacks * slices * 6 * sizeof(unsigned int));


	VAO1.LinkAttrib(VBO1, 0, 3, GL_FLOAT, vertexSize, (void*)0);                      // Pozicije
	VAO1.LinkAttrib(VBO1, 1, 3, GL_FLOAT, vertexSize, (void*)(3 * sizeof(float)));    // Boje

	VAO1.Unbind();
	VBO1.Unbind();
	EBO1.Unbind();

	GLuint uniID = glGetUniformLocation(shaderProgram.ID, "scale");

	float rotation = 0.0f;
	double prevTime = glfwGetTime();

	glEnable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	while (!glfwWindowShouldClose(window))
	{
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		shaderProgram.Activate();
		double crntTime = glfwGetTime();
		if (crntTime - prevTime >= 1 / 60)
		{
			rotation += 0.001f;
			prevTime = crntTime;
		}

		glm::mat4 model = glm::mat4(1.0f);
		glm::mat4 view = glm::mat4(1.0f);
		glm::mat4 proj = glm::mat4(1.0f);

		model = glm::rotate(model, glm::radians(rotation), glm::vec3(0.0f, 1.0f, 0.0f));
		view = glm::lookAt(glm::vec3(0.0f, 0.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		proj = glm::perspective(glm::radians(45.0f), (float)width / height, 0.1f, 10.0f); 

		int modelLoc = glGetUniformLocation(shaderProgram.ID, "model");
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		int viewLoc = glGetUniformLocation(shaderProgram.ID, "view");
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
		int projLoc = glGetUniformLocation(shaderProgram.ID, "proj");
		glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(proj));

		glUniform1f(uniID, 0.5f);
		VAO1.Bind();
		glDrawElements(GL_TRIANGLES, stacks * slices * 6, GL_UNSIGNED_INT, 0);
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	VAO1.Delete();
	VBO1.Delete();
	EBO1.Delete();
	shaderProgram.Delete();
	glfwDestroyWindow(window);
	glfwTerminate();
	return 0;
}
