#version 330 core

layout(location = 0) in vec3 inPosition;  // Pozicija vrha trokuta
layout(location = 2) in vec3 inNormal;    // Normala vrha trokuta

out vec3 FragPos;    // Pozicija fragmenta 
out vec3 Normal;     // Normala fragmenta 

uniform mat4 model;     // Model matrica
uniform mat4 view;      // Matrica pogleda
uniform mat4 proj;      // Matrica projekcije
uniform vec3 lightPosition;  // Pozicija svjetla

void main()
{
    //  pozicija vrha i normala 
    vec4 worldPosition = model * vec4(inPosition, 1.0);
    FragPos = worldPosition.xyz;
    Normal = normalize(mat3(transpose(inverse(model))) * inNormal);

    //  pozicija vrha u koordinatnom prostoru kamere
    vec4 viewPosition = view * worldPosition;

    //  pozicija vrha u koordinatnom prostoru projekcije
    gl_Position = proj * viewPosition;
}
