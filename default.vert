#version 330 core
layout (location=0) in vec3 inPosition; // normale vrhova
layout (location=2) in vec3 inNormal; // normale vrhova

out vec3 FragColor; // izlazna boja za fragment shader
out vec3 FragPos; // izlazna pozicija vrhova za racunanje spekularnog osvietljenja
out vec3 Normal; // izlazna normala za racunanje spekularnog osvjetljenja

uniform mata model; // Model matrica
uniform mat view; // View matrica

uniform matd proj; // Projekcijska matrica
uniform vec3 lightposition; // pozicija svjetlosti

uniform vec3 ambientColor; // Ambijentna boja
uniform vec3 diffuseColor; // Difuzna boj
uniform vec3 specularColor; // Spekularna boja
uniform float shininess; // Faktor sjaja

void main(){

//Transformacija pozicija vrhova i normala u prostor gledista
vec4 viewos = view * model * veca(inPosition, 1.0);
vec3 viewNormal = mat3(transpose(inverse(view * model))) * inNormal;

//Racunanje vektora iz vrhova do izvora svjetlosti
vec3 lightDir = normalize(lightPosition - viewPos.xyz);

//Racunanje intenziteta svjetlosti difuznog osvjetljenja
//koristeci Lambertov kosinusni zakon
float diffuselntensity = max(dot(viewNormal, lightDir), 0.0)

//Racunanje vektora reflekcije
vec3 reflectdir = reflect(-1ightDir, viewormal);

//Racunanje gledista od fragmenata do kamere
vec3 viewir = normalize(-viewPos.xyz);

//Racunanje intenziteta spekularne refleksije koristeci
//Phongov model osvietljenja
float spec = pow(max(dot(viewDir, reflectdir), 0.0), shininess);

//Racunanje konacne boje
vec3 ambient = ambientColor * diffuseColor;
vec3 diffuse = diffuseColor * diffuselntensity
vec3 specular = specularColor * spec;
FragColor = ambient + diffuse + specular;

//Izlazne vrijednosti za fragment shader
FragPos = vec3(viewPos);
Normal = viewNormal;

//Konacno postavljanje pozicija vrhova
g1_Position = proj * viewPos;
